CFLAGS = -std=gnu11 -Wall -Wextra -g
CC = gcc

.INTERMEDIATE: ths.o

ths: ths.o
	$(CC) $(LDFLAGS) -o $@ $<
	sudo setcap cap_sys_admin=+ep $@

clean:
	$(if $(wildcard ths),rm -f ths)
