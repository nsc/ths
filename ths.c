#define _GNU_SOURCE
#define DEBUG
#include <assert.h>
#include <errno.h>
#include <error.h>
#include <sched.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/utsname.h>

#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); } while (0)

struct mnt_opts {
	const char *dir;
	const char *target;
	int move : 1;
	int non_recursive : 1;
	int nosuid : 1;
	int read_only : 1;
};

#define mnt(...) _mnt((struct mnt_opts) { __VA_ARGS__ })

static void _mnt(struct mnt_opts opts)
{
	int flags = MS_BIND;

	assert(opts.dir);

	flags |= opts.read_only ? MS_RDONLY : 0;
	flags |= opts.move ? MS_MOVE : 0;
	flags |= opts.nosuid ? MS_NOSUID : 0;
	flags |= opts.non_recursive ? 0 : MS_REC;

	if (!opts.target)
		opts.target = opts.dir;

	/* mount --bind /bin /bin */
	if (mount(opts.dir, opts.target, NULL, flags|MS_PRIVATE, NULL) < 0)
		errExit(opts.target);

	/* mount --bind -o remount,ro /bin */
	if (opts.read_only &&
	    (mount(NULL, opts.target, NULL, MS_REMOUNT|flags, NULL) < 0))
		errExit(opts.target);
}

static void mnt_tmpfs(const char *dir)
{
	assert(dir);

	/* mount -t tmpfs -o size=100m tmpfs */
	if (mount(NULL, dir, "tmpfs", 0, NULL) < 0)
		errExit(dir);
}

static void env_add(char ***_env, const char *fmt, ...)
{
	char **env = *_env;
	int count = 0;
	va_list va;

	va_start(va, fmt);

	if (!*_env) {
		count = 2;
		*_env = malloc(sizeof(char **) * count);
	} else {
		while (env[count])
			count++;

		count += 2;
		*_env = realloc(*_env, sizeof(char **) * count);
	}

	assert(*_env);

	env = *_env;
	vasprintf(&env[count-2], fmt, va);
	env[count-1] = NULL;

	va_end(va);
}

static void env_copy(char ***_env, const char *name)
{
	char *value = getenv(name);

	if (value)
		env_add(_env, "%s=%s", name, value);
}

static char *get_shell(void)
{
	return strdup(getenv("SHELL"));
}

int main(int argc, char **argv)
{
	char *ths_home;
	char *home;
	char *prog;

	if (unshare(CLONE_NEWNS /*| CLONE_NEWNET | CLONE_NEWIPC */) < 0)
		errExit("unshare");

	/* mount --make-rslave */
	if (mount(NULL, "/", NULL, MS_PRIVATE | MS_REC, NULL) < 0)
		errExit("mount slave");
	mnt(.read_only = 1, .dir = "/");
	mnt(.read_only = 1, .dir = "/sbin");
	mnt(.read_only = 1, .dir = "/lib");
	mnt(.read_only = 1, .dir = "/lib64");
	mnt(.read_only = 1, .dir = "/usr");
	mnt(.read_only = 1, .dir = "/etc");

	home = strdup(getenv("HOME") ? : "/");
	ths_home = getenv("THS_HOME") ? strdup(getenv("THS_HOME")) : NULL;

	if (ths_home) {
		error(0, 0, "Mounting %s to %s.", ths_home, home);

		mnt(.move = 1, .nosuid = 1, .dir = ths_home, .target = home);
		free(ths_home);
	} else {
		error(0, 0, "Creating temporary home directory %s.",
			home);
		mnt_tmpfs(home);
	}

	mnt_tmpfs("/tmp");

	chdir(getenv("HOME"));
	if (argc < 2) {
		prog = get_shell();
		execlp(prog, prog, NULL);
	} else {
		char **env = NULL;

		if (!strcmp(argv[1], "--shell"))
			prog = argv[1] = strdup(getenv("SHELL"));

		prog = argv[1];

		env_add(&env, "HOME=%s", home);
		env_copy(&env, "DISPLAY");
		env_copy(&env, "USER");
		env_copy(&env, "UID");

		execvpe(argv[1], &argv[1], env);
	}

	error(1, errno, prog);
}
